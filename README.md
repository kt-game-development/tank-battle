# Tank Battle

A future Sci-Fi based tank combat game. It takes place in an open world environment playing against an AI. The game is built with the UE4 and is part of an online C++ Udemy course.

## Game Design Document

#### Rules
* You can move anywhere except the steep surrounding mountains or across water.
* Both players start with the same, finite health.
* Both players have infinite ammo but takes time to reload.
* The last player standing is the winner.